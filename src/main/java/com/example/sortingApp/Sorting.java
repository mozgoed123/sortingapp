package com.example.sortingApp;

public class Sorting {
    public static void sort(int[] array) {
        if (array == null) throw new IllegalArgumentException("Null array");
        if (array.length == 0) throw new IllegalArgumentException("Empty array");
        if (array.length == 1) throw new IllegalArgumentException("Single argument array cannot be sorted");
        if (array.length > 10) throw new IllegalArgumentException("More than 10 elements");

        int n = array.length;
        for (int j = 1; j < n; j++) {
            int key = array[j];
            int i = j - 1;
            while ((i > -1) && (array[i] > key)) {
                array[i + 1] = array[i];
                i--;
            }
            array[i + 1] = key;
        }
    }
}