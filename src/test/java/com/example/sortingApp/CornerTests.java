package com.example.sortingApp;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class CornerTests {
    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testNull() {
        int[] array = null;
        sorting.sort(array);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test0Elements() {
        int[] array = {};
        int[] expected = {};
        sorting.sort(array);
        assertArrayEquals(expected, array);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1Element() {
        int[] array = {5};
        int[] expected = {5};
        Sorting.sort(array);
        assertArrayEquals(expected, array);
    }

    @Test
    public void test10Elements() {
        int[] array = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        sorting.sort(array);
        assertArrayEquals(expected, array);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test10Plus() {
        int[] array = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        sorting.sort(array);
        assertArrayEquals(expected, array);
    }

    @Test
    public void testSorted() {
        int[] sorted = {1, 2, 3, 4, 5, 6, 7};
        int[] unsorted = {7, 6, 5, 4, 3, 2, 1};
        int[] expected = {1, 2, 3, 4, 5, 6, 7};

        sorting.sort(sorted);
        assertArrayEquals(expected, sorted);

        sorting.sort(unsorted);
        assertArrayEquals(expected, unsorted);
    }
}
