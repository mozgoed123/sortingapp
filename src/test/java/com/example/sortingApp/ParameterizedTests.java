package com.example.sortingApp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ParameterizedTests {
    private final int[] array;
    private final String expected;

    public ParameterizedTests(int[] array, String expected) {
        this.array = array;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{1, 3, 5, 7, 2, 4, 6}, "[1, 2, 3, 4, 5, 6, 7]"},
                {new int[]{6, 9, 3, 1, 10, 5, 4, 8, 2, 7}, "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"},
                {new int[]{143, 6432, 453, 82, 284, 662, 8957, 5, 616, 72}, "[5, 72, 82, 143, 284, 453, 616, 662, 6432, 8957]"},
        });
    }

    @Test
    public void testSorting() {
        Arrays.sort(array);
        String result = Arrays.toString(array);
        Assert.assertEquals(expected, result);
    }
}


